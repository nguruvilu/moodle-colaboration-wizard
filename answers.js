// Question object start
function Question(questionForm) {
  const SERVER_ADDRESS = "https://api.hybridoma.lt/review";

  function getUsername() {
    let userName = null;

    // Locate User Menu
    let userMenu = document.querySelector('[aria-label="User menu"]');
    if (!userMenu) throw "No User Menu";

    // userName may be in different Elements depending on moodle version
    let userElement = userMenu.querySelector(".usertext");
    if (userElement) {
      userName = userElement.textContent;
    } else {
      userName = userMenu.title;
    }

    if (userName && userName.length > 0) return userName;
    else throw "No User Name";
  }

  function getAnswersInputList(questionForm) {
    function getAllInputs(element) {
      return element.querySelectorAll(
        'input[type="radio"], input[type="checkbox"]'
      );
    }

    let answersInputs = null;

    let answersElement =
      questionForm.querySelector(".answer") ||
      questionForm.querySelector(".multichoice_answer");
    if (answersElement) {
      answersInputs = getAllInputs(answersElement);
    }

    return answersInputs; // May be null
  }

  function getQuestionText(questionForm) {
    // Get rid of usless elements

    let questionFormCopy = questionForm.cloneNode(true);

    let formulasPartElement = questionFormCopy.querySelector(".formulaspart");
    if (formulasPartElement) {
      formulasPartElement.remove();
    }

    let htmlScriptsElement = questionFormCopy.querySelectorAll("script");
    if (htmlScriptsElement) {
      htmlScriptsElement.forEach((htmlScript) => htmlScript.remove());
    }

    // For Lessons || For Quizes
    let questionTextElement = null;
    questionTextElement = questionFormCopy.querySelector(".qtext");

    let questionText = null;
    if (questionTextElement) {
      questionText = questionTextElement.textContent.trim();
    }

    if (questionText.length > 0) return questionText;
    else throw "No Question Text";
  }

  function getAnswerCorrectness(answerInputs) {
    /*
        --- Data structure of answers ---
        answers: [
            {
                checked: 'True or False value',
                answer: 'Answer Text Goes Here'
            },
            ...]
        */

    function getCorrectness(answerInput) {
      cList = answerInput.parentElement.classList;
      for (let i = 0; i < cList.length; i++) {
        if (cList[i] == "correct") return "V";
        else if (cList[i] == "incorrect") return "X";
      }
      return "?";
    }

    let answersList = [];

    answerInputs.forEach((answerInput) => {
      let answerDict = {};

      answerDict["correct"] = getCorrectness(answerInput);
      answerDict["type"] = answerInput.type;
      answerDict["answer"] = getAnswerText(answerInput);

      answersList.push(answerDict);
    });

    if (answersList.length > 0) return answersList;
    // else throw "No Answers"
  }

  function getAnswerText(answerInput) {
    function removeNumberedElement(element) {
      let answerNumberElement = element.querySelector(".answernumber");
      if (answerNumberElement) {
        answerNumberElement.remove();
      }
    }

    function removeSpecificFeedback(element) {
      let answerSpecificFeedback = element.querySelector(".specificfeedback");
      if (answerSpecificFeedback) {
        answerSpecificFeedback.remove();
      }
    }

    let answerText = null;

    let answerInputParentClone = answerInput.parentElement.cloneNode(true);

    removeNumberedElement(answerInputParentClone);
    removeSpecificFeedback(answerInputParentClone);

    answerText = answerInputParentClone.textContent.trim();

    if (answerText && answerText.length > 0) return answerText;
    else throw "No Answer Text";
  }

  function getParsedQuestionData(questionText, answerCorrectness, userName) {
    // Data Validation
    if (!questionText || questionText.length == 0) {
      return;
    }
    if (!answerCorrectness || answerCorrectness.length == 0) {
      return;
    }
    if (!userName || userName.length == 0) {
      throw "No User Name";
    }

    let questionData = {};

    questionData["qtext"] = questionText;
    questionData["corrects"] = answerCorrectness;
    questionData["user"] = userName;

    return questionData;
  }

  function sendReview(dataToSend) {
    /*
        --- Structure of json body data ---
        {
            qtext: 'Question Text Goes Here',
            answers: [
                {
                    checked: 'True or False value',
                    answer: 'Answer Text Goes Here'
                },
                {
                    checked: ...,
                    answer: ...
                },
                ...,
            ],
            user: 'User Name Goes Here'
        }
        */

    // ToDo: Add Debug Option Checkbox

    // logDebugData()

    let dbResponse = fetch(SERVER_ADDRESS, {
      method: "POST",
      body: JSON.stringify(dataToSend),
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((dataReceived) => {
        // do stuff
        return dataReceived;
      });
    return dbResponse;
  }

  // Fun Part
  this.questionForm = questionForm;

  this.userName = getUsername();
  this.answersInputList = getAnswersInputList(this.questionForm);
  this.questionText = getQuestionText(this.questionForm);
  this.answerCorrectness = getAnswerCorrectness(this.answersInputList);

  let dataToSend = getParsedQuestionData(this.questionText, this.answerCorrectness, this.userName)
  if (dataToSend) {
    console.log(dataToSend)
    sendReview(dataToSend)
  }
}
// Question object ends

// Initiate
!(function () {
  function getQuestionDivs() {
    let questionDivs = document.querySelectorAll("div[id^=question-]");
    if (questionDivs.length > 0) return questionDivs;
    else throw "No questiondis found in form.mform";
  }

  let questionForms = getQuestionDivs();

  questionForms.forEach((questionForm) => {
    new Question(questionForm);
  });
})();

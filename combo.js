// Question object start
function Question(questionForm, testType) {

    const SERVER_ADDRESS = 'https://api.hybridoma.lt/';
    const INJECTION_CLASS_NAME = "lsmu_is_best";

    function getUsername() {

        let userName = null

        // Locate User Menu
        let userMenu = document.querySelector('[aria-label="User menu"]')
        if (!userMenu) throw "No User Menu"

        // userName may be in different Elements depending on moodle version
        let userElement = userMenu.querySelector(".usertext")
        if (userElement) { userName = userElement.textContent }
        else { userName = userMenu.title }

        if (userName && userName.length > 0) return userName
        else throw "No User Name"

    }

    function getAnswersInputList(questionForm, testType) {

        function getAllInputs(element) { return element.querySelectorAll('input[type="radio"], input[type="checkbox"]') }

        let answersInputs = null

        // For Lessons || For Quizes
        if (testType == "quiz") {
            let answersElement = questionForm.querySelector('.answer') || questionForm.querySelector('.multichoice_answer')
            if (answersElement) { answersInputs = getAllInputs(answersElement) }
        }
        if (testType == "lesson") { answersInputs = getAllInputs(questionForm) }

        return answersInputs // May be null

    }

    function getQuestionText(questionForm, testType) {

        // Get rid of usless elements

        let questionFormCopy = questionForm.cloneNode(true)

        let formulasPartElement = questionFormCopy.querySelector('.formulaspart')
        if (formulasPartElement) { formulasPartElement.remove() }

        let htmlScriptsElement = questionFormCopy.querySelectorAll('script')
        if (htmlScriptsElement) { htmlScriptsElement.forEach(htmlScript => htmlScript.remove()) }

        // For Lessons || For Quizes
        let questionTextElement = null
        if (testType == "quiz") { questionTextElement = questionFormCopy.querySelector('.qtext') }
        if (testType == "lesson") { questionTextElement = questionFormCopy.querySelector('.contents') }

        let questionText = null
        if (questionTextElement) { questionText = questionTextElement.textContent.trim() }

        if (questionText.length > 0) return questionText
        else throw "No Question Text"

    }

    function getAnswerChoices(answerInputs, testType) {

        /*
        --- Data structure of answers ---
        answers: [
            {
                checked: 'True or False value',
                answer: 'Answer Text Goes Here'
            },
            ...]
        */

        let answersList = []

        answerInputs.forEach(answerInput => {

            let answerDict = {}

            answerDict['checked'] = answerInput.checked
            answerDict['type'] = answerInput.type
            answerDict['answer'] = getAnswerText(answerInput, testType)

            answersList.push(answerDict)

        })

        if (answersList.length > 0) return answersList
        // else throw "No Answers"

    }

    function getAnswerText(answerInput, testType) {

        function removeInjectedElement(element) {
            let injectedElement = element.querySelector(`.${INJECTION_CLASS_NAME}`)
            if (injectedElement) { injectedElement.remove() }
        }

        function removeNumberedElement(element) {
            let answerNumberElement = element.querySelector(".answernumber")
            if (answerNumberElement) { answerNumberElement.remove() }
        }

        let answerText = null

        if (testType == "quiz") {

            let answerInputParentClone = answerInput.parentElement.cloneNode(true)

            removeInjectedElement(answerInputParentClone)
            removeNumberedElement(answerInputParentClone)

            answerText = answerInputParentClone.textContent.trim()

        }

        if (testType == "lesson") {

            let answerInputLabelClone = answerInput.labels[0].cloneNode(true)

            // Do numbered questions in lessons exist ?

            removeInjectedElement(answerInputLabelClone)

            answerText = answerInputLabelClone.textContent.trim()

        }

        if (answerText && answerText.length > 0) return answerText
        else throw "No Answer Text"

    }

    function getParsedQuestionData(questionText, answerChoices, userName) {

        // Data Validation
        if (!questionText || questionText.length == 0) { return }
        if (!answerChoices || answerChoices.length == 0) { return }
        if (!userName || userName.length == 0) { throw "No User Name" }

        let questionData = {}

        questionData["qtext"] = questionText;
        questionData["answers"] = answerChoices;
        questionData["user"] = userName;

        return questionData

    }

    function addInjection(answersInputList, testType) {

        // function getUserInjectionPreference() {
        //     chrome.storage.sync.get(["showInjection"], function (userPreferenceResponse) {
        //         useUserPreference(userPreferenceResponse)
        //     })
        // }

        function useUserPreference(userPreferenceResponse) {
            answersInputList.forEach(answerInput => {
                let injectedElement = getInjectedElement(answerInput, testType)
                if (!injectedElement) {
                    let node = document.createElement("div");
                    node.className = INJECTION_CLASS_NAME
                    let textnode = document.createTextNode("[-]");
                    if (userPreferenceResponse) { node.hidden = !userPreferenceResponse.showInjection }
                    node.appendChild(textnode);
                    let injectionParent = getInjectionParent(answerInput, testType)
                    injectionParent.appendChild(node);
                }
            })
        }

        // getUserInjectionPreference()
        useUserPreference()

    }

    function getInjectionParent(answerInput, testType) {

        let injectionParent = null

        if (testType == "quiz") { injectionParent = answerInput.parentElement }
        if (testType == "lesson") { injectionParent = answerInput.labels[0] }

        if (injectionParent) return injectionParent
        else throw "Do not know where to inject :("

    }

    function getInjectedElement(answerInput, testType) {

        let injectionParent = getInjectionParent(answerInput, testType)

        if (injectionParent === null) throw "Injection Undetermined -> searchElement Not Found"

        let injectedElement = injectionParent.querySelector(`.${INJECTION_CLASS_NAME}`)

        return injectedElement

    }

    function exchangeData(dataToSend, answersInputList, testType) {

        /*
        --- Structure of json body data ---
        {
            qtext: 'Question Text Goes Here',
            answers: [
                {
                    checked: 'True or False value',
                    answer: 'Answer Text Goes Here'
                },
                {
                    checked: ...,
                    answer: ...
                },
                ...,
            ],
            user: 'User Name Goes Here'
        }
        */

        // ToDo: Add Debug Option Checkbox
        function logDebugData() {
            console.log("-----> Debug Text Start <-----")
            // console.log(dataToSend)
            console.log("--- qtext ---")
            console.log(dataToSend.qtext)
            console.log("--- answers ---")
            dataToSend.answers.forEach(answer => { console.log(answer.checked, answer.answer) })
            console.log("-----> Debug Text End <-----")
        }

        // logDebugData()

        let dbResponse = fetch(SERVER_ADDRESS, {
            method: 'POST',
            body: JSON.stringify(dataToSend),
            headers: { 'Content-Type': 'application/json' }
        })
            .then(response => response.json())
            .then(dataReceived => {
                injectExchangedData(dataReceived, answersInputList, testType)
                return dataReceived
            })
        return dbResponse

    }

    function injectExchangedData(data, answersInputList, testType) {

        /*
        --- data structure ---
        data [ [{answer: "answer_text", count: "5"}, {...}, ...] ]
        */

        data[0].forEach(answer => {

            answersInputList.forEach(answerInput => {

                let answerText = getAnswerText(answerInput, testType)

                if (answerText == answer['answer']) {

                    let injectedElement = getInjectedElement(answerInput, testType)

                    if (injectedElement) { injectedElement.textContent = `[${answer['count']}]` }
                    else throw "Injection not Found"

                }
            })
        })

    }


    function prettyPrint(thisss) {
        console.log("--->", { "questionText": thisss.questionText })
        thisss.dataReceived.then(function (results) {
            results[0].forEach(function (result) {
                console.log(result)
            })
            console.log("----------")
        })
    }

    // Fun Part
    this.questionForm = questionForm
    this.testType = testType
    this.userName = getUsername()
    this.answersInputList = getAnswersInputList(this.questionForm, this.testType)
    this.questionText = getQuestionText(this.questionForm, this.testType)

    addInjection(this.answersInputList, this.testType)

    // Listen for Clicks on each Input
    this.answersInputList.forEach(function (answerInput) {
        answerInput.addEventListener('click', function () {

            // Get Answer Choices -> Parse Question Data -> If Data OK -> Send Data
            this.answerChoices = getAnswerChoices(this.answersInputList, this.testType)
            this.parsedQuestionData = getParsedQuestionData(this.questionText, this.answerChoices, this.userName)
            if (this.parsedQuestionData) {
                this.dataReceived = exchangeData(this.parsedQuestionData, this.answersInputList, this.testType)
                this.dataSent = this.parsedQuestionData
            }

            // Debuging
            // console.log(this)
            prettyPrint(this)

        }.bind(this))
    }.bind(this))
}
// Question object ends


// Returns questionForms and testType
function getBasicQuestionData() {

    function getQuizQuestionForms() {
        // May be many questions in one page
        let responseForm = document.querySelector('form#responseform')
        if (!responseForm) throw "No #responseform found"

        let questionForms = responseForm.querySelectorAll("[id*='question']")

        if (questionForms.length > 0) return questionForms
        else throw "No questions found in #responseform"
    }

    function getLessonQuestionForms() {
        let questionForms = [document.querySelector('form.mform')]
        if (questionForms[0].length > 0) return questionForms
        else throw "No questions found in form.mform"
    }


    // let windowLocation = window.location.href

    // For Debugging from Test Files
    let nav = document.querySelector('ol.breadcrumb').querySelectorAll('li')
    let windowLocation = nav[nav.length - 1].querySelector('a').href

    let testType = null
    let questionForms = null

    if (windowLocation.indexOf("/mod/quiz/") > -1) {
        testType = "quiz"
        questionForms = getQuizQuestionForms()
    }
    else if (windowLocation.indexOf("/mod/lesson/") > -1) {
        testType = "lesson"
        questionForms = getLessonQuestionForms()
    }
    else throw "Unsuported/Unknown Test Type"

    return {
        "questionForms": questionForms,
        "testType": testType
    }
}

// Initiate
!(function () {

    // returns questionForms and testType
    let basicQuestionData = getBasicQuestionData()

    let questionForms = basicQuestionData['questionForms']
    let testType = basicQuestionData['testType']

    questionForms.forEach(questionForm => {
        new Question(questionForm, testType)
    })

})()